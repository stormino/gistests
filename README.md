# HOWTO
* Convert spatial reference from EPSG:25832 to EPSG:4326 using reproject:
``` type HikingTrails.json | reproject --use-epsg-io --from=EPSG:25832 --to=EPSG:4326 > h.geojson ```

* Split geojson in many small chunks each of it with 1000 features using geojsplit:
``` geojsplit -a 3 -v -l 1000 h.geojson ```